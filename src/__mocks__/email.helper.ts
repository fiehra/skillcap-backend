export class MockedEmailHelper {
  async sendVerificationEmail(email: string, token: string, hostUrl: string) {
    // Mock implementation, you can customize this as needed.
    return {
      statusCode: 202, // Mimic a successful response
      // Add other properties as needed
    };
  }
  async sendLoginNotificationEmail(email: string, ip: string, userAgent: string) {
    // Mock implementation, you can customize this as needed.
    return {
      statusCode: 202, // Mimic a successful response
      // Add other properties as needed
    };
  }
}
