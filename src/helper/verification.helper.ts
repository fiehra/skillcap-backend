import { autoInjectable } from 'tsyringe';
import VerifyToken from '../models/verifyToken';
import * as crypto from 'crypto';

@autoInjectable()
export class VerificationHelper {
  constructor() {}

  createToken(userId) {
    let token = new VerifyToken({
      userId: userId,
      token: crypto.randomBytes(16).toString('hex').slice(0, 16),
      expireAt: Date.now() + 60 * 60 * 3 * 1000,
    });
    return token;
  }
}
