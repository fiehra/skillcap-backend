import { autoInjectable } from 'tsyringe';

@autoInjectable()
export class DateHelper {
  constructor() {}

  calcExpirationDate(date, duration) {
    let expirationDate = new Date(date);
    expirationDate.setSeconds(expirationDate.getSeconds() + duration);
    return expirationDate;
  }

  // addDays(date, days) {
  //   let result = new Date(date);
  //   result.setDate(result.getDate() + days);
  //   return result;
  // }
}
