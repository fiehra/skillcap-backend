import bcrypt from 'bcrypt';
import User from '../../models/user';
import jwt from 'jsonwebtoken';
import mongoose from 'mongoose';
import { Request, Response } from 'express';
import { EmailHelper } from '../../helper/email.helper';
import { autoInjectable } from 'tsyringe';
import { UserRepository } from './user.repository';
import { VerificationHelper } from '../../helper/verification.helper';
import { UrlHelper } from '../../helper/url.helper';
import { VerificationRepository } from '../verification/verification.repository';
import { DateHelper } from '../../helper/date.helper';

@autoInjectable()
export class UserService {
  constructor(
    private userRepository: UserRepository,
    private emailHelper: EmailHelper,
    private verificationHelper: VerificationHelper,
    private urlHelper: UrlHelper,
    private verificationRepository: VerificationRepository,
    private dateHelper: DateHelper,
  ) {}

  async signupUser(req: Request, res: Response) {
    try {
      const user = new User({
        _id: new mongoose.Types.ObjectId(),
        email: req.body.email,
        username: req.body.username,
        password: await bcrypt.hash(req.body.password, 10),
      });
      const usercheck = await this.userRepository.findOne({
        $or: [{ email: req.body.email }, { username: req.body.username }],
      });
      if (usercheck?.username === req.body.username) {
        return res.status(401).json({
          message: 'username already exists',
        });
      } else if (usercheck?.email === req.body.email) {
        return res.status(400).json({
          message: 'email already taken',
        });
      } else {
        const result = await this.userRepository.save(user);
        if (result) {
          const hostUrl = this.urlHelper.getHostUrl(req);
          const token = this.verificationHelper.createToken(result._id);
          await this.verificationRepository.save(token);
          const sendGridResponse = await this.emailHelper.sendVerificationEmail(
            req.body.email,
            token,
            hostUrl,
          );
          if (sendGridResponse?.statusCode === 202) {
            return res.status(201).json({
              userId: result._id,
              token: token.token,
              message: 'user created',
            });
          } else {
            return res.status(500).json({
              message: 'email not sent',
            });
          }
        }
      }
    } catch (error) {
      return res.status(500).json({
        message: 'signup failed',
      });
    }
  }

  async loginUser(req: Request, res: Response) {
    try {
      const fetchedUser = await this.userRepository.findOne({
        username: req.body.username,
      });
      if (!fetchedUser) {
        return res.status(404).json({
          message: 'user not found',
        });
      } else if (!fetchedUser.verified) {
        return res.status(401).json({
          message: 'not verified',
        });
      } else if (fetchedUser.verified) {
        const result = await bcrypt.compare(req.body.password, fetchedUser.password);
        if (!result) {
          return res.status(401).json({
            message: 'wrong credentials',
          });
        }
        const token = jwt.sign(
          {
            userId: fetchedUser._id,
            username: fetchedUser.username,
            email: fetchedUser.email,
            role: fetchedUser.role,
          },
          process.env.JWT_SECRET,
          { expiresIn: '3h' },
        );
        // makse sure the expiration date matches the expiresIn object in the JWT token
        const expirationDate = this.dateHelper.calcExpirationDate(
          new Date(),
          60 * 60 * 3,
        );
        const authDataResponse = {
          token: token,
          userId: fetchedUser._id,
          username: fetchedUser.username,
          email: fetchedUser.email,
          role: fetchedUser.role,
          expirationDate: expirationDate,
        };
        this.emailHelper.sendLoginNotificationEmail(
          fetchedUser.email,
          req.ip,
          req.headers['user-agent'],
        );
        return res.status(200).json({
          message: 'login success',
          authData: authDataResponse,
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: 'login failed',
      });
    }
  }

  async refreshToken(req: Request, res: Response) {
    try {
      const fetchedUser = await this.userRepository.findOne({
        username: req.body.username,
      });
      // check if expirationDate is in the past
      if (new Date(req.body.expirationDate) < new Date()) {
        return res.status(401).json({
          message: 'token expired',
        });
      } else {
        if (!fetchedUser) {
          return res.status(404).json({
            message: 'user not found',
          });
        } else if (!fetchedUser.verified) {
          return res.status(401).json({
            message: 'not verified',
          });
        } else if (fetchedUser.verified) {
          const token = jwt.sign(
            {
              userId: fetchedUser._id,
              username: fetchedUser.username,
              email: fetchedUser.email,
              role: fetchedUser.role,
            },
            process.env.JWT_SECRET,
            { expiresIn: '3h' },
          );
          // makse sure the expiration date matches the expiresIn object in the jwtToken
          const expirationDate = this.dateHelper.calcExpirationDate(
            new Date(),
            60 * 60 * 3,
          );
          const authDataResponse = {
            token: token,
            userId: fetchedUser._id,
            username: fetchedUser.username,
            email: fetchedUser.email,
            role: fetchedUser.role,
            expirationDate: expirationDate,
          };
          return res.status(200).json({
            message: 'refresh token success',
            authData: authDataResponse,
          });
        }

      }
    } catch (error) {
      return res.status(500).json({
        message: 'refresh token failed',
      });
    }
  }
}
