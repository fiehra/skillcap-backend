import express from 'express';
import { UserController } from './user.controller';
import { autoInjectable } from 'tsyringe';

@autoInjectable()
export class UserRoutes {
  router = express.Router();

  constructor(private userController: UserController) {
    this.configureRoutes();
  }

  configureRoutes() {
    this.router.post('/auth/signup', this.userController.signup);
    this.router.post('/auth/login', this.userController.login);
    this.router.post('/auth/refreshToken', this.userController.refreshToken);
  }
}
