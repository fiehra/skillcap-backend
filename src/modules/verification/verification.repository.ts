import { autoInjectable } from 'tsyringe';
import VerifyToken from '../../models/verifyToken';

@autoInjectable()
export class VerificationRepository {
  async save(object) {
    return new VerifyToken(object).save();
  }

  async findOne(query) {
    return VerifyToken.findOne(query);
  }

  async deleteOne(id) {
    return VerifyToken.deleteOne(id);
  }

  async deleteAllExpired() {
    return VerifyToken.deleteMany({ expireAt: { $lt: new Date() } });
  }
}
