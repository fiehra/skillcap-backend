import { UrlHelper } from '../../helper/url.helper';
import { UserRepository } from '../user/user.repository';
import { VerificationHelper } from '../../helper/verification.helper';
import { Request, Response } from 'express';
import { VerificationRepository } from './verification.repository';
import { EmailHelper } from '../../helper/email.helper';
import { autoInjectable } from 'tsyringe';

@autoInjectable()
export class VerificationService {
  constructor(
    private userRepository: UserRepository,
    private verificationHelper: VerificationHelper,
    private verificationRepository: VerificationRepository,
    private emailHelper: EmailHelper,
    private urlHelper: UrlHelper,
  ) {}

  async verifyToken(req: Request, res: Response) {
    try {
      const tokenFromUrl = req.params.token;
      const fetchedToken = await this.verificationRepository.findOne({
        token: tokenFromUrl,
      });
      if (fetchedToken && fetchedToken?.expireAt > Date.now()) {
        const fetchedUser = await this.userRepository.findOne({ _id: fetchedToken.userId });
        if (fetchedUser) {
          fetchedUser.verified = true;
          await this.userRepository.updateOne({ _id: fetchedUser._id }, fetchedUser);
          await this.verificationRepository.deleteAllExpired();
          await this.verificationRepository.deleteOne({ token: tokenFromUrl });
          const frontEndUrl = this.urlHelper.getFrontendUrl(req);
          res.redirect(frontEndUrl + '/authentication/login');
        }
      } else {
        return res.status(404).json({
          message: 'invalid token',
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: 'verifying failed',
      });
    }
  }

  async resendVerificationLink(req: Request, res: Response) {
    try {
      const hostUrl = this.urlHelper.getHostUrl(req);
      const fetchedUser = await this.userRepository.findOne({ email: req.body.email });
      if (fetchedUser && fetchedUser.verified === false) {
        const token = this.verificationHelper.createToken(fetchedUser._id);
        this.verificationRepository.save(token);
        // let url = hostUrl + '/api/verification/verify/' + token.token;
        // console.log(url);
        const sendGridResponse = await this.emailHelper.sendVerificationEmail(
          req.body.email,
          token,
          hostUrl,
        );
        if (sendGridResponse?.statusCode === 202) {
          return res.status(201).json({
            token: token.token,
            message: 'email sent',
          });
        }
      } else if (fetchedUser && fetchedUser.verified === true) {
        return res.status(400).json({
          message: 'user already verified',
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: 'sending link failed',
      });
    }
  }
}
