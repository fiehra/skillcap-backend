import { autoInjectable, container } from 'tsyringe';
import { VerificationService } from './verification.service';

@autoInjectable()
export class VerificationController {
  constructor() {}

  verify(req, res, next) {
    container.resolve(VerificationService).verifyToken(req, res);
  }

  resendLink(req, res, next) {
    container.resolve(VerificationService).resendVerificationLink(req, res);
  }
}
