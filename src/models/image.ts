import { Schema, model } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

export interface ImageInterface {
  id: string;
  filename: string;
  mobileUrl: string;
  desktopUrl: string;
  alt: string;
  word: string;
}

const imageSchema = new Schema<ImageInterface>({
  id: String,
  filename: { type: String, unique: true },
  mobileUrl: { type: String, required: true },
  desktopUrl: { type: String, required: true },
  alt: { type: String, default: '' },
  word: { type: String, default: '' },
});

imageSchema.plugin(uniqueValidator);
export default model('Image', imageSchema);
