import mongoose, { Schema, model } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

export interface VerifyTokenInterface {
  userId: mongoose.Types.ObjectId;
  token: string;
  expireAt: number;
}

const verifyTokenSchema = new Schema<VerifyTokenInterface>({
  userId: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  token: { type: String, required: true },
  expireAt: { type: Number, default: Date.now, expires: 3600 },
});

verifyTokenSchema.plugin(uniqueValidator);
export default model('VerifyToken', verifyTokenSchema);
