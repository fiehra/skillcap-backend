project setup:

// yarn

development:

1. clone project <br>
   https: git clone https://gitlab.com/fiehra/skillcap-backend.git<br>
   ssh: git@gitlab.com:fiehra/skillcap-backend.git<br>

2. yarn install

3. add dev.env in src directory

// xxx.env template:

PORT=<XXXX> <br>
MONGO=<mongo-connection> <br>
JWT_SECRET=<yourJWTSecretKey> <br>
NODE_ENV=<nodeEnvironment> <br>
EMAIL=<senderEmail> <br>
SENDGRID_API_KEY=<sendgridApiKey> <br>

4. yarn server to start development backend application

test:

1. add test.env
2. yarn test
